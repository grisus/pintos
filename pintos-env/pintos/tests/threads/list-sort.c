#include "stdio.h"
#include "threads/malloc.h"

#include "kernel/list.h"
#include "tests/threads/listpop.h"

struct priority_item {
  // list-specific field
  struct list_elem elem;
  // my fields
  int priority;
};

// the actual test
void test_priority(void);

static void populate(struct list* l, int* a, int n){
  int i;
  struct priority_item * item;

  for (i = 0; i < n; i++) {
    item = malloc(sizeof(struct priority_item));
    item->priority = a[i];
    list_push_back(l, &item->elem);
  }
}

static bool priority_cmp(const struct list_elem * a, const struct list_elem * b, void * aux) {
  if(aux==0) aux=0;
  struct priority_item * left, * right;
  left = list_entry(a, struct priority_item, elem);
  right = list_entry(b, struct priority_item, elem);
  return left->priority < right->priority;
}

static void print_sorted(struct list * l){
  struct list_elem * pos;     // used to iterate
  struct priority_item * pi;  // to retrieve an item

  list_sort(l, priority_cmp, NULL);

  for (pos = list_begin(l); pos != list_end(l); pos = list_next(pos)) {
    pi = list_entry(pos, struct priority_item, elem);   // current position, type of entry, name of struct list_elem's field
    printf("%d ", pi->priority);
  }
  printf("\n");
}

static void list_free(struct list* l) {
  struct list_elem * pos;
  struct priority_item * pi;

  while (!list_empty(l)) {
    pos = list_pop_front(l);
    pi = list_entry(pos, struct priority_item, elem);
    free(pi);
  }
}

void test_priority() {
  struct list l;
  list_init(&l);
  populate(&l, ITEMARRAY, ITEMCOUNT);
  print_sorted(&l);
  list_free(&l);
}
