#include <stdio.h>
#include <string.h>
#include <syscall-nr.h>

#include "devices/shutdown.h"
#include "threads/interrupt.h"
#include "threads/palloc.h"
#include "threads/thread.h"
#include "threads/vaddr.h"
#include "userprog/process.h"
#include "userprog/syscall.h"

static void syscall_handler(struct intr_frame *);
typedef void (*handler)(struct intr_frame *);

// 20 different ones in the syscall-nr.h
static handler call[20];

static void syscall_exit(struct intr_frame *f) {
  int *esp = f->esp;
  struct thread *t = thread_current();
  t->exit_status = *(esp + 1);
  thread_exit();
}

static void syscall_write(struct intr_frame *f) {
  int *esp = f->esp;
  putbuf(*(esp + 2), *(esp + 3));
  f->eax = *(esp + 3);
}

void syscall_init(void) {
  intr_register_int(0x30, 3, INTR_ON, syscall_handler, "syscall");

  // Keep open for future
  call[SYS_EXIT] = syscall_exit;    // Terminate this process.
  call[SYS_WRITE] = syscall_write;  // Write to a file.
}

static void syscall_handler(struct intr_frame *f) {
  int syscall = *((int *)f->esp);
  call[syscall](f);
}
