# Use an official Ubuntu 14.04 as a base image
FROM ubuntu:14.04

# Set the hostname
RUN echo "pintosvm" > /etc/hostname

# Update packages and install necessary software
RUN apt-get update && apt-get install -y gcc g++ gdb binutils \
     libxrandr2 libxrandr-dev \
     libncurses5-dev libncurses5 \
     make zsh curl git 
RUN echo "PATH=/pintos-env/bin/:\$PATH \nPATH=/pintos-env/pintos/utils:\$PATH \nexport PATH \nalias ls='ls --color'" > /root/.zshenv

# Set a default command or entry point if necessary
CMD ["/bin/zsh"]

